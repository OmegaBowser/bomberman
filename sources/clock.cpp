//
// Clock.cpp for Clock in /home/guerri_c//bomberman
// 
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
// 
// Started on  Mon May  6 15:33:52 2013 cedric guerrier
// Last update Tue Jun  4 16:34:19 2013 romain desmarecaux
//

#include	"clock.hh"

int		main(void)
{
  gdl::Clock	myClock;
  
  myClock.play();

  myClock.update();
  std::cout << "Total game time elapsed : " << myClock.getElapsedTime() << " seconde(s)" << std::endl;
  std::cout << "Total time elapsed : " << myClock.getTotalElapsedTime() << " seconde(s)" << std::endl;

  myClock.update();
  std::cout << "Total game time elapsed : " << myClock.getElapsedTime() << " seconde(s)" << std::endl;
  std::cout << "Total time elapsed : " << myClock.getTotalElapsedTime() << " seconde(s)" << std::endl;

  myClock.pause();

  myClock.update();
  std::cout << "Total game time elapsed : " << myClock.getElapsedTime() << " seconde(s)" << std::endl;
  std::cout << "Total time elapsed : " << myClock.getTotalElapsedTime() << " seconde(s)" << std::endl;

  return EXIT_SUCCESS;
}
