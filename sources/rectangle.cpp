//
// rectangle.cpp for Rectangle in /home/guerri_c//bomberman/sources
// 
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
// 
// Started on  Fri May 17 11:30:40 2013 cedric guerrier
// Last update Tue Jun  4 16:50:39 2013 romain desmarecaux
//

#include	"rectangle.hh"

Rectangle::Rectangle(int x, int y, int z)
{
  this->x = x;
  this->y = y;
  this->z = z;
}

void		Rectangle::initialize(void)
{
  this->texture_ = gdl::Image::load("./images/grass3.jpg");
}

void		Rectangle::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;
}

void		Rectangle::draw(void)
{
  this->texture_.bind();
  
  glBegin(GL_QUADS);

  glColor3f(1.0f, 1.0f, 1.0f);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, 250.0f + this->z);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, -250.0f + this->z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, -250.0f + this->z);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, 250.0f + this->z);

  glEnd();
}
