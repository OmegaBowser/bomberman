//
// Music.cpp for bomberman in /home/desmar_r//Projets/C++/bomberman
// 
// Made by romain desmarecaux
// Login   <desmar_r@epitech.net>
// 
// Started on  Tue May 28 13:33:05 2013 romain desmarecaux
// Last update Fri Jun  7 16:17:53 2013 romain desmarecaux
//

#include	"mygame.hh"

Music::Music()
{
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) < 0)
    std::cout << Mix_GetError() << std::endl;
  Mix_AllocateChannels(32);
}

void	Music::playMusic(std::string path)
{
  path = "musics/" + path;
  this->music = Mix_LoadMUS(path.c_str());
  Mix_PlayMusic(this->music, -1);
}

void	Music::playSound(std::string path)
{
  Mix_Chunk	*sound;

  path = "sounds/" + path;
  sound = Mix_LoadWAV(path.c_str());
  Mix_PlayChannel(1, sound, 0);
}

void	Music::stopMusic()
{
  Mix_FreeMusic(this->music);
}

Music::~Music()
{
}
