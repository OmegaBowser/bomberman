//
// model.cpp for model in /home/guerri_c//bomberman
//
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
//
// Started on  Tue May 28 14:49:36 2013 cedric guerrier
// Last update Sat Jun  8 17:11:25 2013 benoit vittaz
//

#include "aobject.hh"
#include <math.h>
#include "ModelException.hpp"
#include "cube.hh"
#include "mygame.hh"

void Bomberman::bombEverywhere(int x, int y, int z)
{
  AObject *bomb;
  bomb = new Bomb(x, y, z);
  this->bombs_.push_back(bomb);
}


void	Bomberman::initialize(void)
{
  try
    {
      this->save_time = 0.0;
      this->zoom = 685;
      this->speed_ = 50;
      glMatrixMode(GL_MODELVIEW);
      try
	{
	  this->model_ = gdl::Model::load("libgdl_gl-2012.4/assets/marvin.fbx");
	}
      catch (...)
	{
	  std::cerr << "Error on loading model" << std::endl;
	  exit(EXIT_FAILURE);
	}
      this->model_.cut_animation(this->model_, "Take 001", 0, 0, "Idle");
      this->model_.cut_animation(this->model_, "Take 001", 35, 54, "Walk");
    }
  catch (const gdl::ModelException *e)
    {
      std::cout << e->what() << std::endl;
    }
}

Bomberman::Bomberman()
{
  this->position_.x = 0;
  this->position_.y = 0;
  this->position_.z = 0;
  this->explo = 0;
  this->zoom = 15;
  this->dezoom = -15;
}


Bomberman::Bomberman(int x, int y, int z, int haut, int large, std::vector<std::string> map)
{
  this->i_die = false;
  this->position_.x = x;
  this->position_.y = y;
  this->position_.z = z;
  this->explo = 0;
  this->zoom = 15;
  this->dezoom = -15;
  this->limit_up = 0;
  this->limit_down = haut;
  this->limit_left = 0;
  this->limit_right = large;
  this->map = map;
}

Bomberman::~Bomberman(void)
{
  delete &this->model_;
}

int	Bomberman::getPositionx() const
{
  return (this->position_.x);
}

int	Bomberman::getPositiony() const
{
  return (this->position_.y);
}

int	Bomberman::getPositionz() const
{
  return (this->position_.z);
}

int	Bomberman::checkCollisionU()
{
  float	y = 0.0;

  for (std::vector<std::string>::iterator it = this->map.begin() ; it != this->map.end() ; it++)
    {
      for (float i = 0.0 ; i < (*it).size() ; i++)
	{
	  if (((this->position_.z + 220.0)  >= y * 500.0) &&
	      ((this->position_.z + 220.0)  <= (y+1) * 500.0) &&
	      ((this->position_.x + 250.0)  >= i * 500.0) &&
	      ((this->position_.x + 250.0)  <= (i+1) * 500.0) &&
	      (*it).at(i) != '0')
	    return (1);
	   }
      y++;
    }
  return (0);
}

int	Bomberman::checkCollisionD()
{
  float	y = 0.0;

  for (std::vector<std::string>::iterator it = this->map.begin() ; it != this->map.end() ; it++)
    {
      for (float i = 0.0 ; i < (*it).size() ; i++)
	{
	  if (((this->position_.z + 280.0)  >= y * 500.0) &&
	      ((this->position_.z + 280.0)  <= (y+1) * 500.0) &&
	      ((this->position_.x + 250.0)  >= i * 500.0) &&
	      ((this->position_.x + 250.0)  <= (i+1) * 500.0) &&
	      (*it).at(i) != '0')
	    return (1);
	}
      y++;
    }
  return (0);
}

int	Bomberman::checkCollisionL()
{
  float	y = 0.0;

  for (std::vector<std::string>::iterator it = this->map.begin() ; it != this->map.end() ; it++)
    {
      for (float i = 0.0 ; i < (*it).size() ; i++)
	{
	  if (((this->position_.z + 250.0)  >= y * 500.0) &&
	      ((this->position_.z + 250.0)  <= (y+1) * 500.0) &&
	      ((this->position_.x + 220.0)  >= i * 500.0) &&
	      ((this->position_.x + 220.0)  <= (i+1) * 500.0) &&
	      (*it).at(i) != '0')
	    return (1);
	}
      y++;
    }
  return (0);
}

int	Bomberman::checkCollisionR()
{
  float	y = 0.0;

  for (std::vector<std::string>::iterator it = this->map.begin() ; it != this->map.end() ; it++)
    {
      for (float i = 0.0 ; i < (*it).size() ; i++)
	{
	  if (((this->position_.z + 250.0)  >= y * 500.0) &&
	      ((this->position_.z + 250.0)  <= (y+1) * 500.0) &&
	      ((this->position_.x + 280.0)  >= i * 500.0) &&
	      ((this->position_.x + 280.0)  <= (i+1) * 500.0) &&
	      (*it).at(i) != '0')
	    return (1);
	}
      y++;
    }
  return (0);
}

int	Bomberman::isDecor()
{
  float	y = 0.0;

  for (std::vector<std::string>::iterator it = this->map.begin() ; it != this->map.end() ; it++)
    {
      for (float i = 0.0 ; i < (*it).size() ; i++)
	{
	  if (((this->posz + 250.0)  >= y * 500.0) &&
	      ((this->posz + 250.0)  <= (y+1) * 500.0) &&
	      ((this->posx + 220.0)  >= i * 500.0) &&
	      ((this->posx + 220.0)  <= (i+1) * 500.0) &&
	      ((*it).at(i) == '2'|| (*it).at(i) == '1'))
	    return (1);
	}
      y++;
    }
  return (0);
}

void	Bomberman::cubeDetect()
{
  float	y = 0.0;
  int cpt1 = 1;
  int cpt2 = 1;

  for (std::vector<std::string>::iterator it = this->map.begin() ; it != this->map.end() ; it++)
    {
      for (float i = 0.0 ; i < (*it).size() ; i++)
	{
	  if (((this->posz + 250.0)  >= y * 500.0) &&
	      ((this->posz + 250.0)  <= (y+1) * 500.0) &&
	      ((this->posx + 220.0)  >= i * 500.0) &&
	      ((this->posx + 220.0)  <= (i+1) * 500.0) &&
	      (*it).at(i) == '2')
	    {
	      (*it).at(i) = '0';
	      g_delete_cube = cpt1;
	    }
	  if ((*it).at(i) == '2' || (*it).at(i) == '1')
	    cpt1++;
	  cpt1++;
	}
      cpt2++;
      y++;
    }
}

void	Bomberman::timer()
{
  if ((this->time - this->time_c >= 0.2) && !this->explosion_.empty())
    this->explosion_.pop_front();
  if ((this->i_die == true) && (this->time - this->time_c) >= 0.2 && g_game_over == -1)
    g_game_over = 0;
}

void	Bomberman::time_game()
{
  int	entier;
  float teatime;

  entier = (int)(this->time * 10.0);
  if (10.0 * this->time - (double)entier >= 0.5)
    ++entier;
  teatime = entier / 10;
  if (this->save_time != teatime)
    this->save_time = teatime;
}

void	Bomberman::die()
{
  if ((((int)((this->position_.x + 250) / 500) * 500) == this->posx)
      && (((int)((this->position_.y + 250) / 500) * 500) == this->posy)
      && (((int)((this->position_.z + 250) / 500) * 500) == this->posz))
      this->i_die = true;
}

void	Bomberman::explosion(gdl::GameClock const & gameClock, Music *sound)
{
  if (!this->bombs_.empty())
    {
      AObject *cube;
      while ((this->posx < (this->savex + 1500)) && Bomberman::isDecor() != 1)
	{
	  cube = new Cube(this->posx, this->posy, this->posz, 3);
	  this->explosion_.push_back(cube);
	  Bomberman::die();
	  this->posx += 500;
	  if (Bomberman::isDecor() == 1)
	    Bomberman::cubeDetect();
	}
      this->posx = this->savex;
      while ((this->posx > (this->savex - 1500)) && Bomberman::isDecor() != 1)
	{
	  cube = new Cube(this->posx, this->posy, this->posz, 3);
	  this->explosion_.push_back(cube);
	  Bomberman::die();
	  this->posx -= 500;
	  if (Bomberman::isDecor() == 1)
	  Bomberman::cubeDetect();
	}
      this->posx = this->savex;
      while ((this->posz < (this->savez + 1500)) && Bomberman::isDecor() != 1)
	{
	  cube = new Cube(this->posx, this->posy, this->posz, 3);
	  this->explosion_.push_back(cube);
	  Bomberman::die();
	  this->posz += 500;
	  if (Bomberman::isDecor() == 1)
	  Bomberman::cubeDetect();
	}
      this->posz = this->savez;
      while ((this->posz > (this->savez - 1500)) && Bomberman::isDecor() != 1)
	{
	  cube = new Cube(this->posx, this->posy, this->posz, 3);
	  this->explosion_.push_back(cube);
	  Bomberman::die();
	  this->posz -= 500;
	  if (Bomberman::isDecor() == 1)
	  Bomberman::cubeDetect();
	}
      std::list<AObject*>::iterator itb4 = this->explosion_.begin();
      for (; itb4 != this->explosion_.end(); ++itb4)
	(*itb4)->initialize();
      this->bombs_.pop_front();
      this->time_c = gameClock.getTotalGameTime();
      this->explo = 1;
      sound->playSound("explosion.wav");
    }
}

void	Bomberman::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  Music	sound;

  this->model_.update(gameClock);
  if (input.isKeyDown(gdl::Keys::Escape) == true)
    {
      std::cout << "TOTAL TIME --> " << this->save_time << " s"  << std::endl;
      exit (0);
    }
  if (input.isKeyDown(gdl::Keys::Up) == true)
    {
      if (limit_up < (this->position_.z) && !this->checkCollisionU())
	{
	  this->position_.z -= 30;
	  glTranslatef(0, 0, 30);
	}
      this->dir_ = UP;
    }
    else if (input.isKeyDown(gdl::Keys::Down) == true)
    {
      if (limit_down > (this->position_.z) && !this->checkCollisionD())
	{
	  this->position_.z += 30;
	  glTranslatef(0, 0, -30);
	}
      this->dir_ = DOWN;
    }
    else if (input.isKeyDown(gdl::Keys::Left) == true)
    {
      if (limit_left < (this->position_.x) && !this->checkCollisionL())
	{
	  this->position_.x -= 30;
	  glTranslatef(30, 0, 0);
	}
      this->dir_ = LEFT;
    }
    else if (input.isKeyDown(gdl::Keys::Right) == true)
    {
      if (limit_right > (this->position_.x) && !this->checkCollisionR())
	{
	  this->position_.x += 30;
	  glTranslatef(-30, 0, 0);
	}
      this->dir_ = RIGHT;
    }
  if (input.isKeyDown(gdl::Keys::PageUp) == true && this->zoom > -2000)
    {
      glTranslatef(0, 20, 0);
      this->zoom -= 20;
    }
  else if (input.isKeyDown(gdl::Keys::PageDown) == true && this->zoom < 6000)
    {
      glTranslatef(0, -20, 0);
      this->zoom += 20;
    }
  if (input.isKeyDown(gdl::Keys::Space) == true && this->bombs_.size() < 1)
    {
      bombEverywhere(getPositionx(), getPositiony(), getPositionz());
      this->time_b = gameClock.getTotalGameTime();
      this->posx = ((int)(this->position_.x + 250.0) / 500) * 500;
      this->savex = posx;
      this->posy = ((int)(this->position_.y + 250.0) / 500) * 500;
      this->savey = posy;
      this->posz = ((int)(this->position_.z + 250.0) / 500) * 500;
      this->savez = posz;
    }
  this->time = gameClock.getTotalGameTime();
  Bomberman::time_game();
  if (this->time - this->time_b >= 1)
    Bomberman::explosion(gameClock, &sound);
  Bomberman::timer();
  std::list<AObject*>::iterator itb3 = this->bombs_.begin();
  for (; itb3 != this->bombs_.end(); ++itb3)
    (*itb3)->initialize();
  if (this->explo == 1)
    {
      sound.playSound("explosion.wav");
      this->explo = 0;
    }
  if (input.isKeyDown(gdl::Keys::Up) == true ||
      input.isKeyDown(gdl::Keys::Down) == true ||
      input.isKeyDown(gdl::Keys::Left) == true ||
      input.isKeyDown(gdl::Keys::Right) == true)
    {
      if (this->explo == 1)
	{
	  sound.playSound("explosion.wav");
	  this->explo = 0;
	}
      this->model_.play("Walk");
    }
  else
    this->model_.play("Idle");
}

void	Bomberman::draw(void)
{
  glPushMatrix();
  std::list<AObject*>::iterator itb3 = this->bombs_.begin();
  for (; itb3 != this->bombs_.end(); ++itb3)
    (*itb3)->draw();

  std::list<AObject*>::iterator itb2 = this->explosion_.begin();
  for (; itb2 != this->explosion_.end(); ++itb2)
    (*itb2)->draw();

  glTranslatef(this->position_.x, this->position_.y, this->position_.z);
  if (this->dir_ == LEFT)
    glRotatef(-90.0f, 0.0f, 20.0f, 0.0f);
  else if (this->dir_ == RIGHT)
    glRotatef(90.0f, 0.0f, 20.0f, 0.0f);
  else if (this->dir_ == DOWN)
    glRotatef(0.0f, 0.0f, 20.0f, 0.0f);
  else if (this->dir_ == UP)
    glRotatef(180.0f, 0.0f, 20.0f, 0.0f);
  gdl::Model::Begin();
  this->model_.draw();
  gdl::Model::End();
  glPopMatrix();
}
