//
// pyramide.cpp for pyramide in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Mon May  6 14:43:27 2013 ludovic benejean
// Last update Tue Jun  4 16:50:24 2013 romain desmarecaux
//

#include	"pyramide.hh"

void		Pyramide::initialize(void)
{
}

void		 Pyramide::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;
}

void		Pyramide::draw(void)
{
  glBegin(GL_TRIANGLES);

  glColor3f(1.0f, 1.0f, 1.0f);
  glVertex3f(0.0f, -150.0f, -150.0f);
  glVertex3f(-150.0f, -150.0f, 150.0f);
  glVertex3f(150.0f, -150.0f, 150.0f);

  glColor3f(1.0f, 0.0f, 0.0f);
  glVertex3f(0.0f, 150.0f, 0.0f);
  glVertex3f(-150.0f, -150.0f, 150.0f);
  glVertex3f(150.0f, -150.0f, 150.0f);

  glColor3f(0.0f, 1.0f, 0.0f);
  glVertex3f(0.0f, 150.0f, 0.0f);
  glVertex3f(0.0f, -150.0f, -150.0f);
  glVertex3f(-150.0f, -150.0f, 150.0f);

  glColor3f(0.0f, 0.0f, 1.0f);
  glVertex3f(0.0f, 150.0f, 0.0f);
  glVertex3f(150.0f, -150.0f, 150.0f);
  glVertex3f(0.0f, -150.0f, -150.0f);
  glEnd();
}
