//
// AObject.cpp for AObject in /home/guerri_c//bomberman
//
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
//
// Started on  Thu May  2 11:06:01 2013 cedric guerrier
// Last update Sat Jun  8 17:12:47 2013 benoit vittaz
//

#include	"aobject.hh"
#include	"mygame.hh"

int		g_delete_cube = -1;
int		g_game_over = -1;

MyGame::MyGame()
{
}

void		MyGame::initialize(void)
{
  AObject	*model;

  createGame();
  frame_ = 0;
  cursor_ = 0;
  isMusic_ = 0;
  score_ = 0;
  window_.setWidth(1500);
  window_.setHeight(1000);
  window_.setTitle("BOMBERMAN");
  window_.create();
  bomberman.initialize();
  bombs.initialize();
  model = new Bomberman(500, 0, 500, this->map_height, this->map_width, this->map);
  bombs.setPosition(bomberman.getPositionx(), bomberman.getPositiony(), bomberman.getPositionz());
  this->objects_.push_back(model);
  std::list<AObject*>::iterator itb2 = this->shelf_.begin();
  for (; itb2 != this->shelf_.end(); ++itb2)
    (*itb2)->initialize();
  std::list<AObject*>::iterator itb = this->objects_.begin();
  for (; itb != this->objects_.end(); ++itb)
    (*itb)->initialize();
}

void		MyGame::update(void)
{
  int cpt = 0;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(0.50f, 0.50f, 95.0f, 1.0f);
  glClearDepth(1.0f);

  if (!g_game_over)
    {
      frame_ = 2;
      isMusic_ = 0;
      g_game_over++;
    }

  std::list<AObject*>::iterator itb = this->objects_.begin();
  for (; itb != this->objects_.end(); ++itb)
    (*itb)->update(gameClock_, input_);
  std::list<AObject*>::iterator itb2 = this->shelf_.begin();
  for (; itb2 != this->shelf_.end(); ++itb2)
    {
      (*itb2)->update(gameClock_, input_);
      if (cpt == g_delete_cube)
	{
	  this->score_ += 10;
	  itb2 = objects_.erase(itb2);
	}
      cpt++;
    }
  g_delete_cube = -1;
  camera_.update(gameClock_, input_);
}

void		MyGame::draw(void)
{
  Music	music;

  if (!frame_)
    {
      if (!isMusic_)
	music.playMusic("mainMenu.ogg");
      isMusic_ = 1;
      this->mainMenu();
    }
  else if (frame_ == 1)
    {
      if (!isMusic_)
	{
	  camera_.initialize();
	  music.playMusic("battle.ogg");
	}
      isMusic_ = 1;

      glPushMatrix();
      glRotatef(0.0f, 0.0f, 0.0f, 0.0f);
      std::list<AObject*>::iterator itb = this->objects_.begin();
      for (; itb != this->objects_.end(); ++itb)
	(*itb)->draw();
      std::list<AObject*>::iterator itb2 = this->shelf_.begin();
      for (; itb2 != this->shelf_.end(); ++itb2)
	(*itb2)->draw();
      this->window_.display();
      glPopMatrix();
    }
  else
    {
      if (!isMusic_)
	music.playMusic("gameOver.ogg");
      isMusic_ = 1;
      this->gameOver();
    }
}

void		MyGame::unload(void)
{
}

void		MyGame::mainMenu()
{
  gdl::Text	text[4];

  text[0].setText("BOMBERSWAG");
  text[1].setText("A");
  text[2].setText("PLAY");
  text[3].setText("QUIT");

  text[0].setFont("./fonts/sega.ttf");
  text[1].setFont("./fonts/arrows.ttf");
  text[2].setFont("./fonts/wildarrows.ttf");
  text[3].setFont("./fonts/wildarrows.ttf");

  text[0].setSize(100);
  text[1].setSize(50);
  text[2].setSize(60);
  text[3].setSize(60);

  text[0].setPosition(350, 100);
  text[1].setPosition(550, 410 + cursor_ * 70);
  text[2].setPosition(650, 400);
  text[3].setPosition(650, 470);

  if (input_.isKeyDown(gdl::Keys::Up) == true && cursor_ > 0)
    cursor_--;
  else if (input_.isKeyDown(gdl::Keys::Down) == true && cursor_ < 1)
    cursor_++;
  else if (input_.isKeyDown(gdl::Keys::Return) == true)
    {
      if (!cursor_)
	{
	  isMusic_ = 0;
	  frame_ = 1;
	}
      else
	exit(EXIT_SUCCESS);
    }

  for (size_t i = 0; i < 4; i++)
    text[i].draw();
}

void	MyGame::gameOver()
{
  std::stringstream ss;
  std::string tmp;
  gdl::Text	text[4];

  text[0].setText("GAME OVER");
  text[1].setText("God admnit, you are not SWAG...");
  text[2].setText("Your score is");
  ss << this->score_;
  ss >> tmp;
  text[3].setText(tmp);

  text[0].setFont("./fonts/sega.ttf");

  text[0].setSize(100);
  text[1].setSize(12);
  text[2].setSize(50);
  text[3].setSize(50);

  text[0].setPosition(450, 400);
  text[1].setPosition(450, 520);
  text[2].setPosition(450, 600);
  text[3].setPosition(750, 600);

  if (input_.isKeyDown(gdl::Keys::Return) == true)
    exit(EXIT_SUCCESS);

  for (size_t i = 0; i < 4; i++)
    text[i].draw();
}
