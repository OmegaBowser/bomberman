//
// bomb.cpp for bomb in /home/beneje_l//bomberman/sources
//
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
//
// Started on  Mon May 13 19:31:24 2013 ludovic benejean
// Last update Sat Jun  8 15:23:20 2013 ludovic benejean
//

#include "bomb.hh"
#include  <iostream>
#include "ModelException.hpp"

void	Bomb::initialize(void)
{
  try
    {
      this->bomb_ = gdl::Model::load("libgdl_gl-2012.4/assets/bomb.fbx");
    }
  catch (const gdl::ModelException *e)
    {
      std::cout << e->what() << std::endl;
    }
}

Bomb::Bomb(int x, int y, int z)
{
  this->x = x;
  this->y = y;
  this->z = z;
}

Bomb::~Bomb(void)
{
  delete &this->bomb_;
}

void	Bomb::setPosition(int x, int y, int z)
{
  this->x = x;
  this->y = y;
  this->z = z;
}


void	Bomb::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  if (input.isKeyDown(gdl::Keys::Space) == true)
    {
    }
}

void	Bomb::draw(void)
{
  glPushMatrix();
  glTranslatef(this->x, this->y, this->z);
  gdl::Model::Begin();
  this->bomb_.draw();
  gdl::Model::End();
  glPopMatrix();
}
