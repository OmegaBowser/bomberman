//
// shelf.cpp for shelf in /home/guerri_c//bomberman/sources
// 
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
// 
// Started on  Thu May 16 10:30:37 2013 cedric guerrier
// Last update Fri Jun  7 16:33:54 2013 cedric guerrier
//

#include	 "mygame.hh"
#include	"rectangle.hh"
#include	"cube.hh"
#include	 "bomb.hh"
#include	"Image.hpp"
#include	"bomberman.hh"
#include	<fstream>

void		MyGame::getMap(std::string file)
{
  file = "maps/" + file;
  std::ifstream	filestream(file.c_str());
  std::string	tmp;

  if (!filestream)
    std::cerr << "Error on retrieveing map" << std::endl;
  else
    {
      while (getline(filestream, tmp))
	this->map.push_back(tmp);
      filestream.close();
    }
}

void		MyGame::createMap()
{
  size_t	empty_case = 0;
  size_t	size;
  size_t	x = 0;
  size_t	y = -1;

  for (std::vector<std::string>::iterator it = this->map.begin(); it != this->map.end(); it++)
    {
      y++;
      for (x = 0; x < (*it).size(); x++)
	{
	  shelfCreate(x, 0, y);
	  if ((*it).at(x) == '1')
	    gameCube(x, 0, y, 1);
	  if ((*it).at(x) == '2')
	    gameCube(x, 0, y, 2);
	  else
	    empty_case++;
	}
    }
  if (x != y + 1)
    {
      std::cerr << "The map must be a square (number of lines = number of columns)." << std::endl;
      exit(EXIT_FAILURE);
    }
  if (empty_case < 4)
    {
      std::cerr << "Map has less than 4 empty cases." << std::endl;
      exit(EXIT_FAILURE);
    }
  for (size_t i = 0; i != map.size(); i++)
    {
      size = 0;
      while (map[0][size])
	{
	  size++;
	} 
      this->map_height = size * 500;
      this->map_width = (i + 1) * 500;
    }
 }

void		MyGame::createGame()
{
  this->getMap("map");
  this->createMap();
}

void		 MyGame::shelfCreate(int x, int y, int z)
{
  AObject	*rectangle;

  rectangle = new Rectangle(x * 500, y * 500, z * 500);
  this->shelf_.push_back(rectangle);
}


void		 MyGame::gameCube(int x, int y, int z, int mode)
{
  AObject	*cube;

  cube = new Cube(x * 500, y, z * 500, mode);
  this->shelf_.push_back(cube);
}
