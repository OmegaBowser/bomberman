//
// main.cpp for main in /home/beneje_l//C++/bomberman
//
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
//
// Started on  Tue Apr 23 13:35:56 2013 ludovic benejean
// Last update Fri May 31 14:44:47 2013 benoit vittaz
//

#include "mygame.hh"
#include "bomberman.hh"

int	main(int ac, char **av, char **env)
{
  av[0] = NULL;
  if (env != NULL && env[0] != NULL)
    {
      if (ac == 1)
	{
	  MyGame game;
	  try
	    {
	      game.run();
	    }
	  catch (const excp &e)
	    {
	      std::cerr << "Error: " << e.what() << "." << std::endl;
	    }
	}
      else
	throw excp ("Usage: ./bomberman");
      return (-1);
    }
  else
    {
      std::cerr << "Error: environment not found." << std::endl;
      return (-1);
    }
}

