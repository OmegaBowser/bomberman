//
// Cube.cpp for Cube in /home/guerri_c//bomberman
//
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
//
// Started on  Mon May  6 15:08:20 2013 cedric guerrier
// Last update Fri Jun  7 16:30:58 2013 cedric guerrier
//

#include	"cube.hh"

Cube::Cube(int x, int y, int z, int text)
{
  this->x = x;
  this->y = y;
  this->z = z;
  this->numText = text;
}

void	Cube::initialize(void)
{
  if (this->numText == 1)
  this->texture_ = gdl::Image::load("./images/Box2.jpg");
  if (this->numText == 2)
  this->texture_ = gdl::Image::load("./images/texture_box.jpg");
  if (this->numText == 3)
    this->texture_ = gdl::Image::load("./images/explosion.jpg");
}

void	Cube::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;
  (void)input;
}

void	Cube::draw(void)
{
  this->texture_.bind();

  glBegin(GL_QUADS);

  glColor3f(1.0f, 0.50f, 0.75f);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-250.0f + this->x, 500.0f + this->y, -250.0f + this->z);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-250.0f + this->x, 500.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(250.0f + this->x, 500.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(250.0f + this->x, 500.0f + this->y, -250.0f + this->z);

  glColor3f(1.0f, 0.50f, 0.75f);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-250.0f + this->x, 500.0f + this->y, 250.0f + this->z);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(250.0f + this->x, 500.0f + this->y, 250.0f + this->z);

  glColor3f(0.0f, 0.0f, 1.0f);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-250.0f + this->x, 500.0f + this->y, -250.0f + this->z);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, -250.0f + this->z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(-250.0f + this->x, 500.0f + this->y, 250.0f + this->z);

  glColor3f(1.0f, 0.0f, 0.0f);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(250.0f + this->x, 500.0f + this->y, -250.0f + this->z);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, -250.0f + this->z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, -250.0f + this->z);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(-250.0f + this->x, 500.0f + this->y, -250.0f + this->z);

  glColor3f(0.0f, 1.0f, 0.0f);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(250.0f + this->x, 500.0f + this->y, 250.0f + this->z);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, -250.0f + this->z);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(250.0f + this->x, 500.0f + this->y, -250.0f + this->z);

  glColor3f(1.0f, 0.50f, 0.75f);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, -250.0f + this->z);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(-250.0f + this->x, 0.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, 250.0f + this->z);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(250.0f + this->x, 0.0f + this->y, -250.0f + this->z);

  glEnd();
}

