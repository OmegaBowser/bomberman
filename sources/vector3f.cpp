//
// vector3f.cpp for vector3f in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Thu May  2 11:30:27 2013 ludovic benejean
// Last update Tue Jun  4 16:51:31 2013 romain desmarecaux
//

#include	 "vector3f.hh"

Vector3f::Vector3f(void) : x(0.0f), y(0.0f), z(0.0f)
{
}

Vector3f::Vector3f(float x, float y, float z) : x(x), y(y), z(z)
{
}
