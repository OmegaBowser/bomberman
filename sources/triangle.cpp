//
// Triangle.cpp for Triangle in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Mon May  6 11:08:16 2013 ludovic benejean
// Last update Tue Jun  4 16:51:23 2013 romain desmarecaux
//

#include	 "triangle.hh"

void		Triangle::initialize(void)
{
}

void		Triangle::update(gdl::GameClock const & gameClock, gdl::Input & input)
{
  (void)gameClock;

  if (input.isKeyDown(gdl::Keys::Right) == true)
    this->rotation_.y = ((int)rotation_.y + 1) % 360;
  if (input.isKeyDown(gdl::Keys::Left) == true)
    this->rotation_.y = ((int)rotation_.y - 1) % 360;

  glRotatef(this->rotation_.y, 0.0f, 1.0f, 0.0f);
}

void		 Triangle::draw(void)
{
  glTranslatef(0.0f, 0.0f, -900.0f);

  glBegin(GL_TRIANGLES);
  glColor3f(1.0f, 0.50f, 0.75f);
  glVertex3f(0.0f, 150.0f, 0.0f);
  glVertex3f(150.0f, 00.0f, 0.0f);

  glEnd();
}
