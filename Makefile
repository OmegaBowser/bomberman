##
## Makefile for Makefile in /home/beneje_l//C++/bomberman
## 
## Made by ludovic benejean
## Login   <beneje_l@epitech.net>
## 
## Started on  Tue Apr 23 13:36:41 2013 ludovic benejean
## Last update Wed May 29 11:44:50 2013 cedric guerrier
##

NAME	=	bomberman

SRC	=	sources/main.cpp \
		sources/mygame.cpp \
		sources/camera.cpp \
		sources/vector3f.cpp \
		sources/rectangle.cpp \
		sources/triangle.cpp \
		sources/cube.cpp \
		sources/pyramide.cpp \
		sources/model.cpp \
		sources/bomb.cpp \
		sources/shelf.cpp \
		sources/music.cpp

OBJ	=	$(SRC:.cpp=.o)

INCLUDES=	-I./libgdl_gl-2012.4/include

LIB	=	-L./libgdl_gl-2012.4/lib -Wl

RPATH	=	--rpath=./libgdl_gl-2012.4/lib/

CPPFLAGS += -W -Wall -Wextra -I./include/ -I./libgdl_gl-2012.4/include/

$(NAME):$(OBJ)
	g++  -o $(NAME) $(OBJ) $(INCLUDES) $(LIB),$(RPATH) -lgdl_gl -lGL -lGLU -lSDL -lSDL_mixer -lm

clean:
	rm -rf $(NAME)

fclean:	clean
	rm -rf $(OBJ)

all:	$(NAME)

re:	fclean all
