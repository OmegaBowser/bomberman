//
// vector3f.hh for vector3f in /home/guerri_c//bomberman
// 
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
// 
// Started on  Thu May  2 12:01:18 2013 cedric guerrier
// Last update Thu May  2 18:34:39 2013 ludovic benejean
//

#ifndef __VECTOR3F_HH__
#define __VECTOR3F_HH__

struct Vector3f
{
  float x;
  float y;
  float z;

  Vector3f(void);
  Vector3f(float x, float y, float z);
};

#endif
