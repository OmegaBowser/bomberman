//
// bomberman.hh for bomberman in /home/beneje_l//C++/bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Tue Apr 23 13:39:49 2013 ludovic benejean
// Last update Fri Jun  7 16:39:57 2013 romain desmarecaux
//

#ifndef __BOMBERMAN_HH__
#define __BOMBERMAN_HH__

#include "Game.hpp"
#include "Clock.hpp"
#include "Color.hpp"
#include "GameClock.hpp"
#include "Image.hpp"
#include "Input.hpp"
#include "Model.hpp"
#include "Window.hpp"
#include "Text.hpp"

#include "exception.hpp"

#include <GL/gl.h>
#include <GL/glu.h>

#include <iostream>
#include <string>
#include <cstdlib>

#endif
