//
// Music.hh for bomberman in /home/desmar_r//Projets/C++/bomberman
// 
// Made by romain desmarecaux
// Login   <desmar_r@epitech.net>
// 
// Started on  Tue May 28 13:34:33 2013 romain desmarecaux
// Last update Tue Jun  4 16:56:17 2013 romain desmarecaux
//

#ifndef		__MUSIC_HH__
#define	       	__MUSIC_HH__

#include	<SDL/SDL_mixer.h>


class		Music
{
public:
  Music();
  ~Music();

  void		playMusic(std::string path);
  void		stopMusic();
  void		playSound(std::string path);

private:
  Mix_Music	*music;
};

#endif
