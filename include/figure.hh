//
// Figure.hh for Figure.hh in /home/guerri_c//bomberman
// 
// Made by cedric guerrier
// Login   <guerri_c@epitech.net>
// 
// Started on  Mon May  6 14:59:38 2013 cedric guerrier
// Last update Tue May  7 15:38:16 2013 ludovic benejean
//

#ifndef __FIGURE_HH__
#define __FIGURE_HH__

class	Triangle : public AObject
{
  void	initialize(void);
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(void);
};
  
class	Rectangle : public AObject
{
  void	initialize(void);
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(void);
};

class	Cube : public AObject
{
  void	initialize(void);
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(void);
};

class	Pyramide : public AObject
{
  void	initialize(void);
  void	update(gdl::GameClock const &, gdl::Input &);
  void	draw(void);
};

#endif
