//
// cube.hh for cube in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Mon May  6 11:58:22 2013 ludovic benejean
// Last update Tue Jun  4 16:54:07 2013 romain desmarecaux
//

#ifndef		__CUBE_HH__
#define		__CUBE_HH__

#include	"aobject.hh"

class		Cube : public AObject
{
public:
  Cube(int, int, int, int);
  void		 initialize(void);
  void		update(gdl::GameClock const &, gdl::Input &);
  void		draw(void);

private:
  gdl::Image	texture_;
  double	dir_;
  int		x;
  int		y;
  int		numText;
  int		z;
};

#endif
