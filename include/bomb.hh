//
// bomb.hh for bomb in /home/beneje_l//bomberman
//
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
//
// Started on  Mon May 13 19:48:00 2013 ludovic benejean
// Last update Tue Jun  4 16:53:01 2013 romain desmarecaux
//

#ifndef		__BOMB_HH__
#define		__BOMB_HH__

#include	"aobject.hh"
#include	"model.hh"

class		Bomb : public AObject
{
public:
  Bomb() {}
  Bomb(int, int, int);
  ~Bomb(void);

  void		initialize(void);
  void		update(gdl::GameClock const &, gdl::Input &);
  void		draw(void);

  void		setPosition(int, int, int);


private:
  gdl::Model	 bomb_;
  Bomberman	bomberman;
  int		x;
  int		y;
  int		z;
  float		time;

};

#endif
