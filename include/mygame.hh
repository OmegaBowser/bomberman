//
// MyGame.hh for MyGame in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Thu May  2 18:26:29 2013 ludovic benejean
// Last update Sat Jun  8 15:39:14 2013 ludovic benejean
//

#ifndef				__MYGAME_HH__
#define				__MYGAME_HH__

#include			<cstdlib>
#include			<list>
#include			<iostream>
#include			<string>
#include			<sstream>
#include			"camera.hh"
#include			"aobject.hh"
#include			"model.hh"
#include			"bomb.hh"
#include			"Game.hpp"
#include			"music.hh"

class				MyGame : public gdl::Game
{
public:
  MyGame();
  void				initialize(void);
  void				update(void);
  void				draw(void);
  void				draw_shelf(void);
  void				unload(void);
  void				shelfCreate(int, int, int);
  void				gameCube(int, int, int, int);
  void				createGame();
  void				getMap(std::string file);
  void				createMap();

  void				mainMenu();  
  void				gameOver();

private:
  Bomb				bombs;
  Bomberman			bomberman;
  Camera			 camera_;
  std::list<AObject*>		objects_;
  std::list<AObject*>		shelf_;
  std::vector<std::string>	map;
  size_t			map_width;
  size_t			map_height;
  size_t			frame_;
  size_t			cursor_;
  size_t			isMusic_;
  int				score_;
};

#endif
