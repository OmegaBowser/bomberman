//
// Camera.hh for Camera in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Thu May  2 11:54:42 2013 ludovic benejean
// Last update Tue Jun  4 16:53:44 2013 romain desmarecaux
//

#ifndef		 __CAMERA_HH__
#define		 __CAMERA_HH__

#include	 "GameClock.hpp"
#include	 "Input.hpp"
#include	 "vector3f.hh"

class		Camera
{
public:
  Camera(void);

  void		initialize(void) const;
  void		update(gdl::GameClock const &, gdl::Input const &) const;

private:
  Vector3f	position_;
  Vector3f	rotation_;
};

#endif
