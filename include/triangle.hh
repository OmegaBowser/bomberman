//
// Triangle.hh for Triangle in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Mon May  6 11:03:50 2013 ludovic benejean
// Last update Tue Jun  4 16:59:00 2013 romain desmarecaux
//

#ifndef		__TRIANGLE_HH__
#define		__TRIANGLE_HH__

#include	"aobject.hh"

class		Triangle : public AObject
{
  void		initialize(void);
  void		update(gdl::GameClock const &, gdl::Input &);
  void		draw(void);
};

#endif
