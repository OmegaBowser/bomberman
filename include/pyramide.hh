//
// pyramide.hh for pyramide in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Mon May  6 12:00:02 2013 ludovic benejean
// Last update Fri Jun  7 16:43:49 2013 romain desmarecaux
//

#ifndef		__PYRAMIDE_HH__
#define		__PYRAMIDE_HH__

#include	 "aobject.hh"

class		Pyramide : public AObject
{
  void		initialize(void);
  void		update(gdl::GameClock const &, gdl::Input &);
  void		draw(void);
};

#endif
