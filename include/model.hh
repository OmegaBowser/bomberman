//
// model.hh for model in /home/beneje_l//bomberman/include
//
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
//
// Started on  Mon May  6 15:45:57 2013 ludovic benejean
// Last update Sat Jun  8 17:11:50 2013 benoit vittaz
//

#ifndef				__MODEL_HH__
#define				__MODEL_HH__

#include			"aobject.hh"
#include			"music.hh"

#define				LEFT	0
#define				RIGHT	1
#define				UP	2
#define				DOWN	3

class				Bomberman : public AObject
{
public:
  Bomberman();
  Bomberman(int, int, int, int, int, std::vector<std::string>);
  ~Bomberman(void);

  void				(Bomberman::*left)();
  void				(Bomberman::*right)();
  void				(Bomberman::*up)();
  void				(Bomberman::*down)();

  void				initialize(void);
  void				update(gdl::GameClock const &, gdl::Input &);
  void				draw(void);
  void				changeDirection(double dir);
  void				bombEverywhere(int, int, int);
  void				timer();
  void				time_game();
  void				cubeDetect();
  void				explosion(gdl::GameClock const & gameClock, Music *sound);
  void				die();

  int				getPositionx()	const;
  int				getPositiony()	const;
  int				getPositionz()	const;

  int				checkCollisionU();
  int				checkCollisionD();
  int				checkCollisionL();
  int				checkCollisionR();

  int				isDecor();

private:
  std::vector<std::string>	map;
  std::list<AObject*>		explosion_;
  std::list<AObject*>		bombs_;
  gdl::Model			model_;
  double			dir_;
  int				speed_;
  int				explo;
  int				x;
  int				y;
  int				z;
  int				zoom;
  int				dezoom;
  float				time;
  float				time_b;
  float				time_c;
  float				save_time;
  int				limit_up;
  int				limit_down;
  int				limit_left;
  int				limit_right;
  int				posx;
  int				posy;
  int				posz;
  int				savex;
  int				savey;
  int				savez;
  int				score;
  bool				i_die;
};

#endif
