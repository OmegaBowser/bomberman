//
// AObject.hh for AObject in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Thu May  2 11:23:24 2013 ludovic benejean
// Last update Fri Jun  7 16:43:57 2013 romain desmarecaux
//

#ifndef		__AOBJECT_HH__
#define		__AOBJECT_HH__

#include	"vector3f.hh"
#include	"bomberman.hh"

extern int		g_delete_cube;
extern int		g_game_over;

class		AObject
{
public:
  AObject(void) : position_(0.0f, 0.0f, 0.0f), rotation_(0.0f, 0.0f, 0.0f), translation_(0.0f, 0.0f, 0.0f)
  {
  }
  virtual void	initialize(void) = 0;
  virtual void	update(gdl::GameClock const &, gdl::Input &) = 0;
  virtual void	draw(void) = 0;
  
protected:
  Vector3f	position_;
  Vector3f	rotation_;
  Vector3f	translation_;
};

#endif
