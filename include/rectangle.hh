//
// Rectangle.hh for Rectangle in /home/beneje_l//bomberman
// 
// Made by ludovic benejean
// Login   <beneje_l@epitech.net>
// 
// Started on  Mon May  6 11:05:43 2013 ludovic benejean
// Last update Tue Jun  4 16:58:40 2013 romain desmarecaux
//

#ifndef		__RECTANGLE_HH__
#define		__RECTANGLE_HH__

#include	 "aobject.hh"

class		Rectangle : public AObject
{
public:
  Rectangle(int, int, int);
  void		initialize(void);
  void		update(gdl::GameClock const &, gdl::Input &);
  void		draw(void);

private:
  gdl::Image	texture_;
  double	dir_;
  int		x;
  int		y;
  int		z;
};

#endif
